Feature: Create files with content

    @setup
    Scenario: Setup a working directory
        Given a new working directory

    Scenario: Create empty files
        Given an empty file named "empty_file"
        Then the file named "empty_file" should exist

    Scenario: Create files with content
        Given a file named "non_empty_file" with:
        """
        Hello World!
        """
        Then the file named "non_empty_file" should exist
        And the file "non_empty_file" should contain "Hello"
        And the file "non_empty_file" should contain
        """
        World
        """

    Scenario: Create files at a directory
        Given an empty file named "my/directory/empty_file"
        Then the file named "my/directory/empty_file" should exist
        And the directory "my/directory" exists

    Scenario: Create files by reusing step within step
        Given a file named "foo" created from within a step with content "foo"
        And a file named "bar" with
            """
            bar
            """
        Then the file named "foo" should exist
        And the file "foo" should contain
        """
        foo
        """
        And the file "bar" should contain
        """
        bar
        """
