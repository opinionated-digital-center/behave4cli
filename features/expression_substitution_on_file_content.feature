Feature: Replace expressions in file content

    Background: Setup a working directory
        Given a new working directory

    Scenario: File content should contain workdir
        When I run "python -c 'with open("foo.txt", "w") as f: import os; f.write("Workdir is {}".format(os.path.abspath(".")))'"
        Then the file "foo.txt" should contain "{__WORKDIR__}"
        And the file "foo.txt" should contain:
            """
            Workdir is {__WORKDIR__}
            """

    Scenario: File content should contain cwd
        When I run "python -c 'with open("foo.txt", "w") as f: import os; f.write(f"Cwd is {os.getcwd()}")'"
        Then the file "foo.txt" should contain "{__CWD__}"
        And the file "foo.txt" should contain:
            """
            Cwd is {__WORKDIR__}
            """

    Scenario: File content should contain today's date in format YYYY-MM-DD
        When I run "python -c 'with open("foo.txt", "w") as f: import datetime; f.write("Today is {}".format(datetime.datetime.today().strftime("%Y-%m-%d")))'"
        Then the file "foo.txt" should contain "{__TODAY_YYYY_MM_DD__}"
        Then the file "foo.txt" should contain:
            """
            Today is {__TODAY_YYYY_MM_DD__}
            """
